# Use a base image with JDK and Maven installed
FROM maven:3.9.6-amazoncorretto-17-debian AS builder

# Set the working directory
WORKDIR /app

# Copy the project files
COPY . .

# Cache Maven dependencies by copying the dependencies file and downloading dependencies
COPY pom.xml .
RUN mvn dependency:go-offline

# Build the application
RUN mvn clean package

# Use a lightweight base image for the runtime
FROM amazoncorretto:8u402-alpine3.19-jre

# Set the working directory
WORKDIR /app

# Copy the built JAR file from the builder stage
COPY --from=builder /app/target/mugiwara-0.0.1-SNAPSHOT.jar .

# Expose the port your application runs on
EXPOSE 8080

# Specify the command to run your application
CMD ["java", "-jar","--spring.profiles.active=prod", "mugiwara-0.0.1-SNAPSHOT.jar"]
