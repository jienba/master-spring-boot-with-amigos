package com.example.demo.exception;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZonedDateTime;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public ResponseEntity<Object> handleException(Exception e) {
        HttpStatus status = determineHttpStatus(e);

        // Build the payload for our client
        ApiException apiException = new ApiException(
                e.getMessage(),
                status,
                ZonedDateTime.now()
        );

        // Send it to our client
        return new ResponseEntity<>(
                apiException,
               status);
    }

    private HttpStatus determineHttpStatus(Exception e) {
        return switch (e.getClass().getSimpleName()) {
            case "ApiRequestException" -> HttpStatus.BAD_REQUEST;
            case "NotFoundException", "MethodArgumentNotValidException" -> HttpStatus.NOT_FOUND;
            default ->
                    HttpStatus.INTERNAL_SERVER_ERROR;
        };
    }
}
