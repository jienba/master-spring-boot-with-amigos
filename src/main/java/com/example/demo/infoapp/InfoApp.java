package com.example.demo.infoapp;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "info.app")
public record InfoApp(String name, String version, String description ) {
} 