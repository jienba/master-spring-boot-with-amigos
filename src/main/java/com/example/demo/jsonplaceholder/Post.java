package com.example.demo.jsonplaceholder;

public record Post(Long userId, Long id, String title, String body) { }
