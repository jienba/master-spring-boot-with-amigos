package com.example.demo.mugiwara;

import java.util.List;


public interface IMugiwaraRepo {
    List<Mugiwara> getMugiwaras();
}
