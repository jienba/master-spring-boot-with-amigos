package com.example.demo.mugiwara;

import com.example.demo.infoapp.InfoApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@EnableConfigurationProperties(InfoApp.class)
public class MugiwaraConfiguration {

    @Value("${app.useFakeMugiwaraRepo:false}")
    private  boolean useFakeMugiwaraRepo;

    @Value("${info.app.description}")
    private String infoAppDescription;

    @Value("${wano.no_kuni: value not set}")
    private String wanoLand;

    @Autowired
    private Environment environment;

    @Bean
    CommandLineRunner commandLineRunner(InfoApp infoApp){
        return args -> {
            System.out.println("Hello Orewa... Orewaaa... Adama D. BA Gozaru 😉😉😉");
            System.out.println("Zeee hahhahahaha ♡ʕ•ᴥ•ʔ♡ "+ this.infoAppDescription);
            System.out.println("Orewaaaa  Kozuki Oden from "+ this.wanoLand);
            System.out.println("Luffy: ... "+ this.environment.getProperty("onePiece.luffyDream"));
            System.out.println("infoApp.toString()");
            System.out.println(infoApp);
        };
    }

    @Bean
    IMugiwaraRepo iMugiwaraRepo(){
        System.out.println("useFakeMugiwaraRepo 🤖 " + this.useFakeMugiwaraRepo);
        return new MugiwaraFakeRepository();
    }
}
