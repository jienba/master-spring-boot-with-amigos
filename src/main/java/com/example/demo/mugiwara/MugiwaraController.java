package com.example.demo.mugiwara;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/mugiwaras")
@Deprecated
public class MugiwaraController {

	private final MugiwaraService mugiwaraService;

	@Autowired
	public MugiwaraController(MugiwaraService mugiwaraService) {
		this.mugiwaraService = mugiwaraService;
	}

	@GetMapping
	public List<Mugiwara> getMugiwaras(){
		return this.mugiwaraService.getMugiwaras();
	}


	@PostMapping
	public  void createNewMugiwara( @RequestBody Mugiwara mugiwara){
		System.out.println("POST REQUEST 🫂🫂🫂");
		System.out.println(mugiwara);
	}

	@PutMapping
	public  void updateMugiwara( @RequestBody Mugiwara mugiwara){
		System.out.println("UPDATE REQUEST 🫂🫂🫂");
		System.out.println(mugiwara);
	}

	@DeleteMapping(path = "{mugiwaraId}")
	void deleteMugiwara( @PathVariable Long mugiwaraId){
		System.out.println("DELETE REQUEST for Mugiwara with ID " + mugiwaraId +" ☠️☠️☠️");
		System.out.println(mugiwaraId);
	}
}
