package com.example.demo.mugiwara;

import com.example.demo.exception.ApiRequestException;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("api/v2/mugiwaras")
@AllArgsConstructor
public class MugiwaraControllerV2 {

    private final MugiwaraService mugiwaraService;

    @GetMapping
    public List<Mugiwara> getMugiwaras() {
        return this.mugiwaraService.getMugiwaras();
    }

    @GetMapping(path = "{mugiwaraId}")
    public ResponseEntity<Mugiwara> getMugiwara(@PathVariable Long mugiwaraId) {
        return ResponseEntity
                .status(HttpStatusCode.valueOf(200))
                .body(this.mugiwaraService.getMugiwara(mugiwaraId));
    }

    @GetMapping(path = "{mugiwaraId}/exception")
    public Mugiwara getMugiwaraException(@PathVariable Long mugiwaraId) {
        throw new ApiRequestException("ApiRequestException for mugiwara " + mugiwaraId);
    }

    @PostMapping
    /*In the context of Spring MVC, when you use @Valid on a method parameter,
    Spring catches these standard validation exceptions internally and translates
    them into a MethodArgumentNotValidException.*/
    public void createNewMugiwara(@Valid @RequestBody Mugiwara mugiwara) {
        System.out.println("POST REQUEST 🫂🫂🫂");
        System.out.println(mugiwara);
    }

    @PutMapping
    public void updateMugiwara(@Valid @RequestBody Mugiwara mugiwara) {
        System.out.println("UPDATE REQUEST 🫂🫂🫂");
        System.out.println(mugiwara);
    }

    @DeleteMapping(path = "{mugiwaraId}")
    void deleteMugiwara(@PathVariable Long mugiwaraId) {
        System.out.println("DELETE REQUEST for Mugiwara with ID " + mugiwaraId + " ☠️☠️☠️");
        System.out.println(mugiwaraId);
    }
}
