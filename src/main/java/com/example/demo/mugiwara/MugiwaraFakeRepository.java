package com.example.demo.mugiwara;

import java.util.List;

public class MugiwaraFakeRepository implements IMugiwaraRepo {
    @Override
    public List<Mugiwara> getMugiwaras() {
        return List.of(
                new Mugiwara(1L, "Luffy", "password123", "luffy@mugiwara.op"),
                new Mugiwara(2L, "Zoro", "Hoo", "zoro@mugiwara.op"),
                new Mugiwara(3L, "Sanji", "password", "sanji@mugiwara.op"),
                new Mugiwara(4L, "Nami", "passwordqa", "nami@mugiwara.op")
        );
    }
}
