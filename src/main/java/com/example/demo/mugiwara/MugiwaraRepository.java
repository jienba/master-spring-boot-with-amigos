package com.example.demo.mugiwara;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MugiwaraRepository extends JpaRepository<Mugiwara, Long> {



}
