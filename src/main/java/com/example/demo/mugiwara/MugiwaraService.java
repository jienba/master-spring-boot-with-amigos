package com.example.demo.mugiwara;

import com.example.demo.exception.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class MugiwaraService {


    private final MugiwaraRepository mugiwaraRepository;

    // By default, the primary implementation is used when it is annotated with @Primary
    // Else we have to specify qualifier @Qualifier("qualifier name") 

    public List<Mugiwara> getMugiwaras() {
        return this.mugiwaraRepository.findAll();
    }

    public Mugiwara getMugiwara(Long mugiwaraId) {
        return this.mugiwaraRepository
                .findById(mugiwaraId)
                .orElseThrow(() -> {
                    var errorMessage = "Mugiwara with id " + mugiwaraId + " not found 😢.";
                    var notFoundException =
                            new NotFoundException(errorMessage);
//                    log.error(errorMessage, notFoundException);
                    log.error(errorMessage);
                    return notFoundException;

                        }
                );
    }
}
