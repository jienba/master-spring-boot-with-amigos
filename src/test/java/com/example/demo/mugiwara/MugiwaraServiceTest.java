package com.example.demo.mugiwara;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@DataJpaTest
class MugiwaraServiceTest {

    @Mock
    private MugiwaraRepository mugiwaraRepository;
    private MugiwaraService underTest;

    @BeforeEach
    void setUp() {
        this.underTest = new MugiwaraService(mugiwaraRepository);
    }

    @AfterEach
    void tearDown() {
        this.mugiwaraRepository.deleteAll();
    }

    @Test
    void getMugiwaras() {
//        given
        Mugiwara luffy = new Mugiwara(
                1L,
                "Monkey D. Luffy",
                "monkey",
                "luffy@mugiwaa.op"
                );
        Mugiwara zoro = new Mugiwara(
                2L,
                "Zoro",
                "swordman",
                "zoro@mugiwara.op");

        List<Mugiwara> mugiwaraList = List.of(luffy, zoro);
//        when
        when(mugiwaraRepository.findAll()).thenReturn(mugiwaraList);

        List<Mugiwara> mugiwaras = this.underTest.getMugiwaras();
//        then

        assertEquals(mugiwaraList.size(), mugiwaras.size());
    }

    @Test
    void getMugiwara() {
        // Given
        Mugiwara sanji = new Mugiwara(
                3L,
                "Sanji",
                "password",
                "sanji@mugiwara.op");

//        mugiwaraRepository.save(sanji);
        // when
        when(mugiwaraRepository.findById(3L)).thenReturn(Optional.of(sanji));

        Mugiwara actualMugiwara = underTest.getMugiwara(3L);
        // then
        assertEquals(sanji.getId(), actualMugiwara.getId());
        assertEquals(sanji.getName(), actualMugiwara.getName());
        assertEquals(sanji.getPassword(), actualMugiwara.getPassword());
        assertEquals(sanji.getEmail(), actualMugiwara.getEmail());

    }
}